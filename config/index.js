const Config = {
    serviceName:  getOrReturnDefaultValue("SERVICE_NAME", "ps_node_content_service"),
    environment: getOrReturnDefaultValue("ENVIRONMENT", "debug"), // debug, test, release
    version: getOrReturnDefaultValue("VERSION", "1.0"),

    serviceHost: getOrReturnDefaultValue("SERVICE_HOST", "localhost"),
    grpcPort: getOrReturnDefaultValue("GRPC_PORT",":9102"),

    mongoHost: getOrReturnDefaultValue("MONGO_HOST", "localhost"),
    mongoPort: getOrReturnDefaultValue("MONGO_PORT", "27017"),
    mongoUser: getOrReturnDefaultValue("MONGO_USER", "node_content_service"),
    mongoPassword: getOrReturnDefaultValue("MONGO_PASSWORD", ""),
    mongoDatabase: getOrReturnDefaultValue("MONGO_DATABASE", "node_content_service"),
};

function getOrReturnDefaultValue(name, def = "") {
    if (process.env[name]) {
        return process.env[name];
    }
    return def;
}

module.exports = Config;

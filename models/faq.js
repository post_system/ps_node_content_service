const mongoose = require("mongoose");
const { v4 } = require("uuid");

const LanguageSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: [true, 'title is REQUIRED'],
        },
        text: {
            type: String,
            // required: [true, 'text is REQUIRED'],
        },
    }
);

const FAQSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            default: v4,
        },
        order_number: {
            type: Number,
            // required: [true, 'order_number is REQUIRED']
        },
        ru: {
            type: LanguageSchema,
        },
        uz_latin: {
            type: LanguageSchema,
        },
       
        uz_kiril: {
            type: LanguageSchema,
        },
    },
    {
        timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
        toObject: {
            virtuals: true
        },
        toJSON: {
            virtuals: true
        }
    }
);

module.exports = mongoose.model("FAQ", FAQSchema);
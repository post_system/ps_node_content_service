const FAQStorage = require("../storage/mongo/faq");
const CatchWrapService = require("../wrappers/service");

const namespace = "Service.FAQ";

const FAQService = {
    Create: CatchWrapService(`${namespace}.Create`, FAQStorage.Create),
    GetByID: CatchWrapService(`${namespace}.GetByID`, FAQStorage.GetByID),
    GetList: CatchWrapService(`${namespace}.GetList`, FAQStorage.GetList),
    Update: CatchWrapService(`${namespace}.Update`, FAQStorage.Update),
    // UpdateFAQOrder: CatchWrapService(`${namespace}.UpdateFAQOrder`, FAQStorage.UpdateFAQOrder),
    Delete: CatchWrapService(`${namespace}.Delete`, FAQStorage.Delete),
};

module.exports = FAQService;

const FAQModel = require("../../models/faq");
const CatchWrapDb = require("../../wrappers/db");

const namespace = "Storage.FAQ";

const FAQStorage = {
    Create: CatchWrapDb(`${namespace}.Create`, async (args) => {
        let countFAQ = await FAQModel.countDocuments();

        let faq = new FAQModel(args);
        faq.order_number = countFAQ + 1;

        const resp = await faq.save();
        return resp;
    }),

    GetByID: CatchWrapDb(`${namespace}.GetByID`, async (args) => {
        if (!args.id) { throw new Error("id is required to get faq") };

        let faq = await FAQModel.findOne({ _id: args.id });

        if (!faq) { throw new Error("failed to find faq with given id") };

        return faq;
    }),

    GetList: CatchWrapDb(`${namespace}.GetList`, async (args) => {
        let query = {};

        if (args.search.trim()) {
            query = {
                ...query,
                $or: [{
                    'uz_latin.title': { $regex: ".*" + args.search + ".*" },
                },
                {
                    'uz_kiril.title': { $regex: ".*" + args.search + ".*" },
                },
                {
                    'ru.title': { $regex: ".*" + args.search + ".*" },
                },
                ], // regex here
            }
        };

        let options = {
            limit: args.limit,
            skip: args.offset
        };

        return {
            faqs: await FAQModel.find(query, {}, options),
            count: await FAQModel.countDocuments(query)
        };
    }),

    Update: CatchWrapDb(`${namespace}.Update`, async (args) => {
        if (!args.id) { throw new Error("id is required to update faq") }

        let faq = await FAQModel.findOneAndUpdate(
            {   //query
                _id: args.id,
            },
            {   //update
                $set: {
                    uz_latin: args.uz_latin,
                    uz_kiril: args.uz_kiril,
                    ru: args.ru
                }
            },
            {   //options
                upsert: false,
                new: true,
            }
        );

        if (!faq) { throw new Error(`faq with given id is not found!`) };

        return faq;
    }),

    // UpdateFAQOrder: CatchWrapDb(`${namespace}.UpdateFAQOrder`, async (args) => {
    //     if (!args.faq.id) { throw new Error("id is required to update faq order") }
    //     if (!args.faq.order_number) { throw new Error("order_number is required to update faq order") }

    //     let faq = await FAQModel.findOne({ _id: args.faq.id });
    //     if (!faq) { throw new Error("failed to find faq with given id") };

    //     let argsOrderNumber = args.faq.order_number
    //     let toDown = faq.order_number < args.faq.order_number

    //     let query = {}, set = {};

    //     query.order_number = (toDown ?
    //         { $lte: argsOrderNumber, $gt: faq.order_number } :
    //         { $gte: argsOrderNumber, $lt: faq.order_number }
    //     );
    //     set = (toDown ?
    //         { $inc: { order_number: -1 } } :
    //         { $inc: { order_number: 1 } }
    //     );

    //     faq = await FAQModel.updateMany(query, set)
    //     faq = await FAQModel.findOneAndUpdate(
    //         {   //query
    //             _id: args.faq.id,
    //         },
    //         {   //update
    //             $set: {
    //                 order_number: argsOrderNumber,
    //             }
    //         },
    //         {   //options
    //             upsert: false,
    //             new: true,
    //         }
    //     );

    //     if (!faq) { throw new Error("failed to find faq with given id") };

    //     return faq;
    // }),

    Delete: CatchWrapDb(`${namespace}.Delete`, async (args) => {
        if (!args.id) { throw new Error("id is required to delete faq") }
        let faq = await FAQModel.findOneAndDelete({ _id: args.id });
        if (!faq) { throw new Error(`faq with given id is not found!`) };

        faq = await FAQModel.updateMany(
            {   //query
                order_number: { $gte: faq.order_number },
            },
            {   //update
                $inc: { order_number: -1 }
            }
        )

        if (!faq) { throw new Error(`faq with given id is not found!`) };

        return {};
    }),
};

module.exports = FAQStorage;
